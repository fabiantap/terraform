FROM ubuntu:22.04
# Actualiza el sistema y instala los paquetes necesarios
WORKDIR /app
RUN apt-get update 
RUN apt-get upgrade -y 
RUN apt-get install -y curl wget unzip 
RUN apt-get install -y git 
RUN apt-get install python3 python3-pip -y
RUN git clone https://github.com/kubernetes-incubator/kubespray.git && \
    cd kubespray && \
    pip install -r requirements.txt
WORKDIR /
COPY terraform.pem /root/.ssh/id_rsa
# Ejecuta el contenedor en modo interactivo
ENTRYPOINT [ "" ]
CMD ["/bin/bash"]

# docker build -t ansible:kubesprite1.0 .
# docker run -it ansible:kubesprite1.0 bash
# ansible --version
# cp -rfp kubespray/inventory/sample inventory/mycluster
# declare -a IPS=(18.206.187.75 3.236.151.24 44.200.135.39 18.206.92.77 44.213.81.155)
# CONFIG_FILE=kubespray/inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}

# docker run terraform:cesar1.8 terraform destroy --auto-approve